# Macbook Setup Notes
## Initial stuff
- Install Slack
- Install Twitter via AppStore
- Install Chrome and login to sync settings
- System Preferences 
  - Dock
    - Set size to small
    - Turn on magnification
    - Turn on auto hide
  - Trackpad
    - Scroll turn off natural direction to allow for more natural scrolling on desktop
  - Setup apple id with icloud setup and other continuity setup like iMessages and Facetime phone calling with iPhone.
- Set Finder to show hidden files and folders:
  - ```defaults write com.apple.Finder AppleShowAllFiles YES && killall -HUP Finder``` 
- Install AppCleaner and Clean My Mac
- Install HomeBrew
  - Install XCode command line utilities
    - ```xcode-select --install```
  - ```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```
- ReOpen terminal for brew to work.
- brew update
  - (shouldn't have anything to update)
- Add iterm2 to Dock and remove terminal if it is there
- setup Bash
  - ```sudo vi /etc/shells```
  - add the path /usr/local/bin/bash
  - comment out the others
  - Change to the new shell
  - ```chsh -s /usr/local/bin/bash```   
  - ```touch .bash_profile```
  - add contents for the .bash_profile
  - Node.js setup
    - ```nvm install stable```
    - ```npm install -g eslint```
  - git setup
    - ```git config --global user.email "mmosttler@harryanddavid.com"```
    - ```git config --global user.name "Marcus Mosttler"```
- Install SDKMan
  - ```curl -s "https://get.sdkman.io" | bash```
  - ```source "$HOME/.sdkman/bin/sdkman-init.sh"```
  - ```sdk install java 8.0.232.j9-adpt```
  - ```sdk install maven 3.6.3```
- Setup Docker
  - 
- Setup Eclipse
  - JDK
- Setup Mongo
  - https://www.thepolyglotdeveloper.com/2019/01/getting-started-mongodb-docker-container-deployment/
- Setup Confluent Platform (kafka)
  - https://docs.confluent.io/current/quickstart/ce-quickstart.html#ce-quickstart
  - ccloud
    - curl -L https://cnfl.io/ccloud-cli | sh -s -- -b /Users/e019904/work/confluent-5.3.2/bin
- Setup System Proxy for work:
```
# Proxy settings
httpProxyServer=user:password@medor-ws01.1800flowers.int:8080
export http_proxy=$httpProxyServer
export https_proxy=$httpProxyServer
export HTTP_PROXY=$httpProxyServer
export HTTPS_PROXY=$httpProxyServer
export no_proxy=localhost,127.0.0.1
```

- 
NOTE: had to add settings to allow local machine name to be resolved
https://crunchify.com/getting-java-net-unknownhostexception-nodename-nor-servname-provided-or-not-known-error-on-mac-os-x-update-your-privateetchosts-file/
# Cryptography and SSL/TLS Toolkit
brew install "openssl@1.1"
# Bourne-Again SHell, a UNIX command interpreter
brew install "bash"
# Programmable completion for Bash 3.2
brew install "bash-completion"
# Fish completion for brew-cask
brew install "brew-cask-completion"
# Top-like interface for container metrics
brew install "ctop"
# Get a file from an HTTP, HTTPS or FTP server
brew install "curl"
# Clean Docker containers, images, networks, and volumes
brew install "docker-clean"
# Bash, Zsh and Fish completion for Docker
brew install "docker-completion"

# Isolated development environments using Docker
brew install "docker-compose", link: false

# Docker-compose completion script
brew install "docker-compose-completion"
# Distributed revision control system
brew install "git"
# Smarter Dockerfile linter to validate best practices
brew install "hadolint"
# Improved top (interactive process viewer)
brew install "htop"
# Lightweight and flexible command-line JSON processor
brew install "jq"
# NCurses Disk Usage
brew install "ncdu"
# Port scanning utility for large networks
brew install "nmap"
# 7-Zip (high compression file archiver) implementation
brew install "p7zip"
# Static analysis and lint tool, for (ba)sh scripts
brew install "shellcheck"
# Executes a program periodically, showing output fullscreen
brew install "watch"
# Internet file retriever
brew install "wget"
brew install nvm

# kubernetes cli
brew install kubectl

brew cask install "docker"
brew cask install "iterm2"
brew cask install "postman"
brew cask install "slack"
brew cask install visual-studio-code
brew cask install "visual-studio-code-insiders"
brew cask install eclipse-java
